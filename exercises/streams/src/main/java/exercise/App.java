package exercise;

import java.util.List;
import java.util.Arrays;


// BEGIN
class App {
    public static void main(String[] args) {

        String[] emails = {
                "info@gmail.com",
                "info@yandex.ru",
                "info@hotmail.com",
                "mk@host.com",
                "support@hexlet.io",
                "key@yandex.ru",
                "sergey@gmail.com",
                "vovan@gmail.com",
                "vovan@hotmail.com"
        };

        List<String> emailsList = Arrays.asList(emails);
        App.getCountOfFreeEmails(emailsList); // 3
        System.out.println(App.getCountOfFreeEmails(emailsList));


    }

    public static int getCountOfFreeEmails(List<String> inputList) {

        List<String> freeDomains = List.of("@hotmail.com", "@gmail.com", "@yandex.ru");

        return (int) inputList.stream()
                .map(s -> s.substring(s.lastIndexOf("@")))
                .filter(freeDomains::contains)
                .count();
    }
}
// END
