package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

    public static int getX(int[] inputCoordinates) {
        return inputCoordinates[0];
    }

    public static int getY(int[] inputCoordinates) {
        return inputCoordinates[1];
    }

    public static String pointToString(int[] inputCoordinates) {
        return "(" + getX(inputCoordinates) + ", " + getY(inputCoordinates) + ")";
    }

    public static int getQuadrant(int[] inputCoordinates) {
        int ruQuadrant = 1; // ru -- right and up
        int luQuadrant = 2; // lu -- left and up
        int ldQuadrant = 3; // ld -- left and down
        int rdQuadrant = 4; // rd -- right and down
        int outOfQuadrant = 0;
        if (getX(inputCoordinates) > 0 && getY(inputCoordinates) > 0) {
            return ruQuadrant;
        } else if (getX(inputCoordinates) < 0 && getY(inputCoordinates) > 0) {
            return luQuadrant;
        } else if (getX(inputCoordinates) < 0 && getY(inputCoordinates) < 0) {
            return ldQuadrant;
        } else if (getX(inputCoordinates) > 0 && getY(inputCoordinates) < 0) {
            return rdQuadrant;
        }
        return outOfQuadrant;
    }

    public static int[] getSymmetricalPointByX(int[] inputCoordinates) {
        int[] symmetrical = new int[2];
        symmetrical[0] = getX(inputCoordinates);
        symmetrical[1] = getY(inputCoordinates) * -1;
        return symmetrical;
    }

    public static int calculateDistance(int[] firstInputCoordinates, int[] secondInputCoordinates) {
        double distance = Math.hypot(getX(secondInputCoordinates) - getX(firstInputCoordinates),
                (getY(secondInputCoordinates) - getY(firstInputCoordinates)));
        return (int) distance;
    }
    // END

}
