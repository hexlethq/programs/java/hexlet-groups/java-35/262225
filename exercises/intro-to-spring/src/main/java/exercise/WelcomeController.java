package exercise;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {

    @GetMapping("/hello")
    @ResponseBody
    public String getName(@RequestParam(defaultValue = "World") String name) {
        return "Hello, " + name;
    }


    @GetMapping("/")
    @ResponseBody
    public String getRoot() {
        return "Welcome to Spring";
    }

}
// END
