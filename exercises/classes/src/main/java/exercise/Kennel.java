package exercise;

import java.util.Arrays;

// BEGIN
public class Kennel {
    public static String[][] allPuppies = new String[0][0];
    public static int puppiesCount = 0;

    public static void addPuppy(String[] Puppy) {
        allPuppies = Arrays.copyOf(allPuppies, puppiesCount + 1);
        allPuppies[puppiesCount] = Puppy;
        puppiesCount = puppiesCount + 1;
    }

    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppy : puppies) {
            addPuppy(puppy);
        }
    }

    public static int getPuppyCount() {
        return allPuppies.length;
    }

    public static boolean isContainPuppy(String name) {
        for (String[] puppy : allPuppies) {
            if (puppy[0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return allPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] names = new String[0];
        int count = 0;

        for (String[] puppysBreed : allPuppies) {
            if (puppysBreed[1].equals(breed)) {
                names = Arrays.copyOf(names, names.length + 1);
                names[count] = puppysBreed[0];
                count = count + 1;
            }
        }
        return names;
    }

    public static void resetKennel() {
        allPuppies = new String[0][0];
    }
}
// END
