package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List<Map<String, String>> getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonToString = Files.readString(Path.of("src/main/resources/users.json"));
        return objectMapper.readValue(jsonToString, new TypeReference<>() {
        });
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        PrintWriter out = response.getWriter();
        StringBuilder result = new StringBuilder();
        result.append("""
                <!DOCTYPE html>
                <html lang=\"ru\">
                    <head>
                        <meta charset=\"UTF-8\">
                        <title>Example application | Users</title>
                        <link rel=\"stylesheet\" href=\"mysite.css\">
                    </head>
                    <body>
                """);
        result.append("""
                <table>""");

        for (Map<String, String> user : users) {
            result.append("<tr>");
            result.append("<td>" + user.get("id") + "</td>");
            result.append("<td><a href=" + "/users/" + user.get("id") + ">" + user.get("firstName") + " " + user.get("lastName"));
            result.append("</a>");
            result.append("</tr>");
        }

        result.append("""                                      
                    </table>
                    </body>
                </html>
                """);

        response.setContentType("text/html;charset=UTF-8");
        out.println(result.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        PrintWriter out = response.getWriter();
        List<Map<String, String>> users = getUsers();
        Map<String, String> result = new HashMap<>();
        for (Map<String, String> user : users)
            if (user.get("id").equals(id)) {
                result = user;
            }

        if (result.isEmpty()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;

        }

        StringBuilder builder = new StringBuilder();
        builder.append("""
                <!DOCTYPE html>
                <html lang=\"ru\">
                    <head>
                        <meta charset=\"UTF-8\">
                        <title>Example application | Users</title>
                        <link rel=\"stylesheet\" href=\"mysite.css\">
                    </head>
                    <body>
                """);
        builder.append("""
                <table>""");


        builder.append("<tr>");
        builder.append("<td>").append(result.get("id")).append("</td>");
        builder.append("<td>").append(result.get("firstName")).append("</td>");
        builder.append("<td>").append(result.get("lastName")).append("</td>");
        builder.append("<td>").append(result.get("email")).append("</td>");
        builder.append("</tr>");
        builder.append("""                                      
                    </table>
                    </body>
                </html>
                """);

        response.setContentType("text/html;charset=UTF-8");
        out.println(builder.toString());


    }
}
