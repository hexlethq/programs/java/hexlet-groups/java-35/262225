package exercise;

import com.google.gson.Gson;

// BEGIN
public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        String[] fruits = {"apple", "pear", "lemon"};
        App.toJson(fruits); // => ["apple","pear","lemon"]
    }

   public static String toJson(String[] inputArr) {

       Gson gson = new Gson();
       String jsonToString = gson.toJson(inputArr);
       return jsonToString;
   }
}
// END
