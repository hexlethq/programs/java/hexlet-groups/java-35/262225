package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        boolean triangleExist = a < b +c && b < a + c && c < a + b; // существует
        boolean versatileTriangle = a != b && b != c && a !=c;   // Разносторонний
        boolean equilateralTriangle = a == b && b == c; // Равносторонний
        boolean isoscelesTriangle = a == c || b == c;   // Равнобедренный
        if (triangleExist) {
            if (versatileTriangle) {
                return "Разносторонний";
            }
            if (equilateralTriangle) {
                return "Равносторонний";
            }
            if (isoscelesTriangle) {
                return "Равнобедренный";
            }
        }
        return "Треугольник не существует";
    }

    // END
}
