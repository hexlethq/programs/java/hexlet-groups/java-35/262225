package exercise;

import java.util.*;

// BEGIN
public class App {
    public static void main(String[] args) {
        List<Map<String, String>> books = new ArrayList<>();

        Map<String, String> book1 = new HashMap<>(
                Map.of("title", "Cymbeline", "author", "Shakespeare", "year", "1611")
        );
        Map<String, String> book2 = new HashMap<>(
                Map.of("title", "Book of Fooos", "author", "FooBar", "year", "1111")
        );
        Map<String, String> book3 = new HashMap<>(
                Map.of("title", "The Tempest", "author", "Shakespeare", "year", "1611")
        );
        Map<String, String> book4 = new HashMap<>(
                Map.of("title", "Book of Foos Barrrs", "author", "FooBar", "year", "2222")
        );
        Map<String, String> book5 = new HashMap<>(
                Map.of("title", "Still foooing", "author", "FooBar", "year", "3333")
        );

        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);

        Map<String, String> where = new HashMap<>(Map.of("author", "Shakespeare", "year", "1611"));

        List<Map<String, String>> result = App.findWhere(books, where);


        System.out.println(result); // =>
// [
//     {title=Cymbeline, year=1611, author=Shakespeare},
//     {title=The Tempest, year=1611, author=Shakespeare}
// ]
    }
    public static List<Map<String, String>> findWhere(List<Map<String, String>> inputList, Map<String, String> inputMap) {

        List<Map<String, String>> result = new ArrayList<>();

        Set<Map.Entry<String, String>> res = inputMap.entrySet();

        for (int i = 0; i < inputList.size(); i++) {
            Map<String, String> x = inputList.get(i);
            Set<Map.Entry<String, String>> res2 = x.entrySet();
            if (res2.containsAll(res)) {
                result.add(x);
            }
        }
        return result;
    }
}



//END
