package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {

        String result = "";

        String[] words = str.trim().split("\\s*(\\s|,|!|\\.)\\s*");
        // данное регулярное выражение нашел в сети, пока только немного понимаю как оно работает,
        // но уже пошёл изучать.
        for (String word : words) {
            result = result + word.substring(0,1).toUpperCase();
        }
        return result;
    }
}
// END

