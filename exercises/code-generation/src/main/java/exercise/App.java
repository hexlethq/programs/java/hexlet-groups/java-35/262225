package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;

// BEGIN
public class App {
    public static void main(String[] args) {

    }

    public static void save(Path path, Car car) {
        byte[] strToBytes = car.serialize().getBytes();
        try {
            Files.write(path,strToBytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Car extract(Path path) {
        try {
            String result = Files.readString(path);
            return Car.unserialize(result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
// END
