package exercise;

// BEGIN
import java.util.HashMap;
import java.util.Map;

public class FileKV implements KeyValueStorage {

    String path;
    Map<String,String> base = new HashMap<>();

    public FileKV(String path, Map<String, String> map) {
        this.path = path;
        base.putAll(map);
        String result = Utils.serialize(map);
        Utils.writeFile(path,result);
    }

    @Override
    public void set(String key, String value) {
        base.put(key, value);
        String result = Utils.serialize(base);
        Utils.writeFile(path,result);

    }

    @Override
    public void unset(String key) {
        base.remove(key);
        String result = Utils.serialize(base);
        Utils.writeFile(path,result);

    }

    @Override
    public String get(String key, String defaultValue) {
        String result = Utils.readFile(path);
        Map<String, String> map = Utils.unserialize(result);
        if(!(map.get(key) == null)) {
            return map.get(key);
        } else return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String,String> result = new HashMap<>();
        result.putAll(base);
        return result;
    }

    @Override
    public String toString() {
        return "" + base;
    }
}
// END
