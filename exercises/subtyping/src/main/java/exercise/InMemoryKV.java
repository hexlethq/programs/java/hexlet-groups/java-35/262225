package exercise;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {

    Map<String, String> base = new HashMap<>();

    public InMemoryKV(Map<String, String> storage) {
        base.putAll(storage);
    }

    @Override
    public void set(String key, String value) {
        base.put(key, value);
    }

    @Override
    public void unset(String key) {
        base.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        if (!(base.get(key) == null)) {
            return base.get(key);
        } else return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String,String> result = new HashMap<>();
        result.putAll(base);
        return result;
    }


}

// END
