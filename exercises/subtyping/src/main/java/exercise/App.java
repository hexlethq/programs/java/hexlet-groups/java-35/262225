package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage inputMap) {
        Map<String, String> map = inputMap.toMap();
        Map<String, String> rev = new HashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            inputMap.unset(entry.getKey());
            inputMap.set(entry.getValue(), entry.getKey());
        }
    }
}
// END
