package exercise;

import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
// BEGIN
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN

    @Test
    void fileKVGetTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value"));
        assertThat(storage.get("key", "default")).isEqualTo("value");
    }

    @Test
    void fileKVSetTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value"));
        Map<String,String> expected = new HashMap<>(Map.of("key","value" , "key5", "value5"));
        storage.set("key5","value5");
        assertThat(storage.toMap()).isEqualTo(expected);
    }

    @Test
    void fileKVUnSetTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value", "key5", "value5"));
        storage.unset("key");
        assertThat(storage.get("key", "def")).isEqualTo("def");
    }




    // END
}
