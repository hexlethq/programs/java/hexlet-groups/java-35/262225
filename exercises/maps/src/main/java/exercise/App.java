package exercise;

import java.util.HashMap;
import java.util.Map;


// BEGIN
public class App {
    public static void main(String[] args) {

    }

    public static Map<String, Integer> getWordCount(String input) {

        String delimeter = " ";
        String[] subStr = input.split(delimeter);
        Map<String, Integer> words = new HashMap<>();
        if (!input.isEmpty()) {
            for (int i = 0; i < subStr.length; i++) {
                int count = 1;
                count = count + words.getOrDefault(subStr[i], 0);
                words.put(subStr[i], count);
            }
            return words;
        }
        return words;
    }

    public static String toString(Map<String, Integer> inputMap) {

        String result = "{";
        String resultForEmptyMap = "{}";

        if (!inputMap.isEmpty()){
            for (Map.Entry<String, Integer> words: inputMap.entrySet()) {
                result = result.concat("\n  " + words.getKey() + ": " + words.getValue());
            }
            result = result.concat("\n}");
            return result;
        }
        return resultForEmptyMap;
    }
}
//END
