package exercise.connections;

import exercise.TcpConnection;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class Connected implements Connection {

    List<String> result = new ArrayList<>();

    private TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error, already connected");
    }

    @Override
    public void disconnect() {
        TcpConnection c = this.tcpConnection;
        c.setConnection(new Disconnected(c));

    }

    @Override
    public void write(String context) {
        result.add(context);

    }
}

// END
