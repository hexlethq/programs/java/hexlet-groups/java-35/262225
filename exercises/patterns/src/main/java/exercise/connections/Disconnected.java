package exercise.connections;

import exercise.TcpConnection;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class Disconnected implements Connection {

    List<String> result = new ArrayList<>();

    private  TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }

    @Override
    public void connect() {
        TcpConnection c = this.tcpConnection;
        c.setConnection(new Connected(c));
    }

    @Override
    public void disconnect() {
        System.out.println("Error, already disconnected");

    }

    @Override
    public void write(String context) {
        System.out.println("Error! Connection disconnected");

    }
}

// END
