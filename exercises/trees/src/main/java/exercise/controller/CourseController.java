package exercise.controller;

import exercise.model.Course;
import exercise.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(path = "")
    public Iterable<Course> getCorses() {
        return courseRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Course getCourse(@PathVariable long id) {
        return courseRepository.findById(id);
    }

    // BEGIN
    @GetMapping(path = "/{id}/previous")
    public Iterable<Course> getParent(@PathVariable Long id) {
        Course course =  courseRepository.findById(id).orElseThrow(RuntimeException::new);
        String parentId = course.getPath();
        if(parentId != null) {
            List<Long> longs = Arrays.stream(parentId.split("\\.")).mapToLong(Integer::parseInt).boxed().toList();
            return courseRepository.findAllById(longs);
        } else return List.of();
    }

    // END

}
