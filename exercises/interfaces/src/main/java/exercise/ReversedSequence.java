package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {

    String str;

    public ReversedSequence(String str) {
        StringBuilder x = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            x.append(str.charAt(i));
        }
        this.str = x.toString();
    }

    @Override
    public int length() {
        return str.length();
    }

    @Override
    public char charAt(int index) {
        return str.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        StringBuilder x = new StringBuilder();
        for (int i = start; i < end; i++) {
            x.append(str.charAt(i));
        }
        return x.toString();

    }

    @Override
    public String toString() {
        return str;
    }
}

// END
