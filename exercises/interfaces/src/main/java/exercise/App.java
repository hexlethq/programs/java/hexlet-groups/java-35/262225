package exercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static void main(String[] args) {

        List<Home> appartments = new ArrayList<>(List.of(
                new Flat(41, 3, 10),
                new Cottage(125.5, 2),
                new Flat(80, 10, 2),
                new Cottage(150, 3)
        ));

        List<String> result = App.buildAppartmentsList(appartments, 3);
        System.out.println(result);

        ///////////////////////////////////////////////////////////

        CharSequence text = new ReversedSequence("abcdef");
        text.toString(); // "fedcba"
        text.charAt(2); // 'e'
        text.length(); // 6
        text.subSequence(1, 4).toString(); // "edc"

    }

    public static List<String> buildAppartmentsList(List<Home> appartments, int count) {

        List<String> result = new ArrayList<>();
        appartments.sort(Comparator.comparing(Home::getArea));

        if (appartments.isEmpty()) {
            return new ArrayList<>();
        }

        for (int i = 0; i < count; i++) {
            Home home = appartments.get(i);
            result.add(home.toString());
        }
        return result;
    }
}
// END
