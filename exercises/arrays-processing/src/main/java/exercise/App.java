package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int valueOfIndex = Integer.MIN_VALUE ;
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < 0 && arr[i] > valueOfIndex) {
                valueOfIndex = arr[i];
                index = i;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] inputArray) {
        int averageArithmetic = 0;
        int sumArrayValue = 0;
        int arrLenght = 0;
        for (int k: inputArray) {
            sumArrayValue += k;
        }

        if (inputArray.length == 0) {
            return inputArray;
        } else averageArithmetic = sumArrayValue / inputArray.length;

        for (int i = 0; i < inputArray.length; i++){
            if (inputArray[i] <= averageArithmetic) {
                arrLenght = arrLenght + 1;
            }
        }

        int[] newArray = new int[arrLenght];

        for (int i = 0; i < inputArray.length; i++){
            if (inputArray[i] <= averageArithmetic) {
                newArray[i] = inputArray[i];
            }
        }
        return newArray;
    }

    public static int getSumBeforeMinAndMax(int[] arr) {

        int minValue = Integer.MIN_VALUE ;
        int maxValue = Integer.MAX_VALUE;
        int indexOfMinValue = 0;
        int indexOfMaxValue = 0;
        int valueOfIndex = 0;
        int sum = 0;

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < maxValue && arr[i] < valueOfIndex) {
                valueOfIndex = arr[i];
                indexOfMinValue = i;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] > minValue && arr[i] > valueOfIndex) {
                valueOfIndex = arr[i];
                indexOfMaxValue = i;
            }
        }

        if (indexOfMaxValue < indexOfMinValue){
            int[] s1 = Arrays.copyOfRange(arr, indexOfMaxValue +1, indexOfMinValue);
            for (int i = 0; i < s1.length; i++){
                sum = sum + s1[i];
            }
        } else if (indexOfMaxValue > indexOfMinValue) {
            int[] s1 = Arrays.copyOfRange(arr, indexOfMinValue + 1, indexOfMaxValue);
            for (int i = 0; i < s1.length; i++){
                sum = sum + s1[i];
            }
        }
        return sum;
    }
    // END
}
