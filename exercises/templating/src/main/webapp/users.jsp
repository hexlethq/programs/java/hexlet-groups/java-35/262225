<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
          crossorigin="anonymous">
</head>
<body>
<% List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users"); %>
<h1>List of users</h1><br>
<% for (Map<String, String> user : users) {%>
<ul>
    <li><p><b>ID:</b>
        <%= user.get("id")%>
    </p></li>
    <li><p><b>Name:</b>
        <a href="/users/show?id=<%= user.get("id")%>"><%= user.get("firstName") + " " + user.get("lastName")%></a>
    </p></li>
    <li><p><b>Email:</b>
        <%= user.get("email")%>
    </p></li>
    <p>----------------------------------------------</p>
</ul>
<%}%>
</body>
</html>

<!-- END -->
