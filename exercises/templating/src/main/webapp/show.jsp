<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
          crossorigin="anonymous">
</head>
<body>
<% Map<String, String> user = (Map<String, String>) request.getAttribute("user"); %>
<% Map<String, String> users = (Map<String, String>) request.getAttribute("users"); %>
<h1><%= user.get("firstName") + " " + user.get("lastName")%></h1><br>
<ul>
    <li><p><b>ID:</b>
        <%= user.get("id")%>
    </p></li>
    <li><p><b>Name:</b>
                <%= user.get("firstName") + " " + user.get("lastName")%>
    </p></li>
    <li><p><b>Email:</b>
        <%= user.get("email")%>
    </p></li>
    <p>----------------------------------------------</p>
    <a href="/users/delete?id=<%= user.get("id")%>">Delete user</a>
</ul>
</body>
</html>

<!-- END -->
