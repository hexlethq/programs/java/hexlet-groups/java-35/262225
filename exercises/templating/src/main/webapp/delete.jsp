<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
          crossorigin="anonymous">
</head>
<body>
<% Map<String, String> user = (Map<String, String>) request.getAttribute("user"); %>
<p></p>
<p></p>
<p></p>
<h2 align="center" style="color:Black">You are sure? Delete <%= user.get("firstName") + " " + user.get("lastName")%>?</h2><br>
    <form style="margin-left : 50vw" action='/users/delete?id=${id}' method="post">
        <button type="submit" class="btn btn-danger">Yes</button>
    </form>
<p></p>
<p></p>
<form style="margin-left : 50vw" action="/users" method="get">
    <button type="submit" class="btn btn-danger">No</button>
</form>
</ul>
</body>
</html>

<!-- END -->
