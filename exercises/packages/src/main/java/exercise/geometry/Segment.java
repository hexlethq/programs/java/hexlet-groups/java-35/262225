// BEGIN
package exercise.geometry;
public class Segment {

    public static double[][] makeSegment(double[] x, double[] y) {
        double[][] segment = {x, y};
        return segment;
    }

    public static double[] getBeginPoint(double[][] x) {
        double[] BeginPoint = x[0];
        return BeginPoint;

    }

    public static double[] getEndPoint(double[][] x) {
        double[] EndPoint = x[1];
        return EndPoint;

    }

}


// END
