// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

import java.util.Arrays;

public class App {

    public static double[] getMidpointOfSegment(double[][] x) {
        double[] result = new double[x.length];
        result[0] = (x[0][0] + x[1][0]) / 2;
        result[1] = (x[0][1] + x[1][1]) / 2;
        return result;
    }

    public static double[][] reverse(double[][] x) {
        double[] point1 = Arrays.copyOf(Segment.getBeginPoint(x),x.length);
        double[] point2 = Arrays.copyOf(Segment.getEndPoint(x),x.length);
        double[][] result = Segment.makeSegment(point2, point1);
        return result;

    }

}
// END
