package exercise;

import java.util.Arrays;

// BEGIN
public class App {

    public static void main(String[] args) throws Exception {

        String example3 = "[foo:bar]\n" +
                "environment=\"X_FORWARDED_var1=111\"\n" +
                "\n" +
                "[bar:baz]\n" +
                "environment=\"key2=true,X_FORWARDED_var2=123\"\n" +
                "command=sudo -HEu tirion /bin/bash -c 'cd /usr/src/app && make prepare'\n" +
                "\n" +
                "[baz:foo]\n" +
                "key3=\"value3\"\n" +
                "command=sudo -HEu tirion /bin/bash -c 'cd /usr/src/app && make prepare'\n" +
                "\n" +
                "[program:prepare]\n" +
                "environment=\"key5=value5,X_FORWARDED_var3=value,key6=value6\"\n" +
                "\n" +
                "[program:start]\n" +
                "environment=\"pwd=/temp,user=tirion\"\n" +
                "\n" +
                "[program:options]\n" +
                "environment=\"X_FORWARDED_mail=tirion@google.com,X_FORWARDED_HOME=/home/tirion,language=en\"\n" +
                "command=sudo -HEu tirion /bin/bash -c 'cd /usr/src/app && make environment'\n" +
                "\n" +
                "[empty]\n" +
                "command=\"X_FORWARDED_HOME=/ cd ~\"";

        System.out.println(getForwardedVariables(example3));
    }


    public static String getForwardedVariables(String inputConf) {
        String[] arr = inputConf.split("\\r?\\n");

        String[] result = Arrays.stream(arr)
                .filter(s -> s.startsWith("environment"))
                .map(s -> s.substring(s.indexOf('"') + 1))
                .map(s -> Arrays.stream(s.split(",")).filter(s1 -> s1.startsWith("X_FORWARDED_")).toArray(String[]::new))
                .flatMap(strings -> Arrays.stream(strings).map(s -> s.substring(s.indexOf('_', s.indexOf('_') + 1) + 1)))
                .map(s -> s.replaceAll("\"", ""))
                .toArray(String[]::new);
        return String.join(",", result);
    }
}
//END
