package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


class App {
    // BEGIN
    public static String buildList(String[] inputArr) {

        StringBuilder str = new StringBuilder();

        if (inputArr.length == 0) {
            return "";
        }

        str.append("<ul>\n");

        for (int i = 0; i < inputArr.length; i++) {

            str.append("  <li>");
            str.append(inputArr[i]);
            str.append("</li>\n");

        }
        str.append("</ul>");

        return str.toString();
    }

    public static String getUsersByYear(String[][] arrOfName, int yearOfBirthday) {

        int arrLenght = 0;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for (int i = 0; i < arrOfName.length; i++) {
            String date = arrOfName[i][1];
            LocalDate localDate = LocalDate.parse(date, formatter);
            int year = localDate.getYear();

            if(yearOfBirthday == year) {
                arrLenght = arrLenght + 1;
            }
        }

        String[] oneArr = new String[arrLenght];

        int placeInArray = 0;
        String result;


        for (int k = 0; k < arrOfName.length; k++) {
            String date = arrOfName[k][1];
            LocalDate localDate = LocalDate.parse(date, formatter);
            int year = localDate.getYear();

            if(yearOfBirthday == year) {
                String name = arrOfName[k][0];
                oneArr[placeInArray] = name;
                placeInArray = placeInArray + 1;
            }
        }
        result = App.buildList(oneArr);
        return result;

    }

    // END

    // Это дополнительная задача, которая выполняется по желанию.
//    public static String getYoungestUser(String[][] users, String date) throws Exception {
//        // BEGIN
//
//        // END
//    }
}
