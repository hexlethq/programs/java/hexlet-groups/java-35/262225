package exercise;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static void main(String[] args) {
        Map<String, Object> data1 = new HashMap<>(
                Map.of("one", "eon", "two", "two", "four", true, "abs", 'h')
        );
        Map<String, Object> data2 = new HashMap<>(
                Map.of("two", "own", "zero", 4, "four", true)
        );

        Map<String, String> result = App.genDiff(data1, data2);
        System.out.println(result);

    }

    public static Map<String, String> genDiff(Map<String, Object> map1, Map<String, Object> map2) {

        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        Set<String> mergeMap = new TreeSet<>();
        mergeMap.addAll(map1.keySet());
        mergeMap.addAll(map2.keySet());

        for (String key : mergeMap) {
            if (!map1.containsKey(key)) {
                result.put(key, "added");
            } else if (!map2.containsKey(key)) {
                result.put(key, "deleted");
            } else if (map1.get(key).equals(map2.get(key))) {
                result.put(key, "unchanged");
            } else result.put(key, "changed");
        }
        return result;
    }
}

//END
