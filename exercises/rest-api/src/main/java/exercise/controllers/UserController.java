package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser().findList();
        String s = DB.json().toJson(users);
        ctx.json(s);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser().id.equalTo(Integer.parseInt(id)).findOne();
        String s = DB.json().toJson(user);
        ctx.json(s);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        String body = ctx.body();
        User newUser = DB.json().toBean(User.class, body);
        newUser.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        String body = ctx.body();
        User userFromReq = DB.json().toBean(User.class, body);
        String firstName = userFromReq.getFirstName();
        String lastName = userFromReq.getLastName();
        String email = userFromReq.getEmail();
        String password = userFromReq.getPassword();
        new QUser().id.equalTo(Integer.parseInt(id))
                .asUpdate().set("firstName", firstName)
                .set("lastName",lastName)
                .set("email", email)
                .set("password",password)
                .update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser().id.equalTo(Integer.parseInt(id)).delete();
        // END
    };
}
