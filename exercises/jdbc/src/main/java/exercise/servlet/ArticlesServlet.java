package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.*;
import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;


public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                              HttpServletResponse response)
            throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        int pageNumber;

        if(request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));

        } else {
            request.setAttribute("page", 1);
            pageNumber = 1;
        }

        List<Map<String, String>> articles = new ArrayList<>();
        String query = "SELECT id,title,body name FROM articles ORDER BY id LIMIT ? OFFSET ?";
            try {
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, 10);
                statement.setInt(2, (pageNumber * 10) - 10);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    articles.add(Map.of(
                                    "id", rs.getString("id"),
                                    "title", rs.getString("title")
                            )
                    );
                }

            } catch (SQLException e) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
        request.setAttribute("articles", articles);
        request.setAttribute("page", pageNumber);
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                             HttpServletResponse response)
            throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        int x = Integer.parseInt(Objects.requireNonNull(getId(request)));
        List<Map<String, String>> articles = new ArrayList<>();
        String query = "SELECT title,body name FROM articles WHERE id= ? ";
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, x);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                articles.add(Map.of(
                                "title", rs.getString("title"),
                                "body", rs.getString("body")
                        )
                );
            }

        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        if(articles.isEmpty()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        request.setAttribute("articles", articles);
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
