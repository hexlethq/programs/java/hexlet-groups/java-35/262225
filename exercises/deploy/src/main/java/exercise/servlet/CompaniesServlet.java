package exercise.servlet;

import exercise.Data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        PrintWriter out = response.getWriter();
        List<String> companies = getCompanies();
        String value = request.getParameter("search");

        if (request.getQueryString() != null || request.getParameter("search") != null) {
            List<String> search = companies.stream()
                    .filter(x -> x.contains(value))
                    .toList();
            if (search.isEmpty()) {
                out.println("Companies not found");
            }
            search.forEach(out::println);
        } else companies.forEach(out::println);

        out.close();
        // END
    }
}
