package exercise;

class Triangle {
    // BEGIN

    public static double getSquare(double firstSide, double secondSide, double angle) {
        double angleToRadian = (angle * Math.PI) / 180;
        double sinus = Math.sin(angleToRadian);
        return ((firstSide * secondSide) / 2 ) * sinus;
     }


         public static void main(String[] args) {
             System.out.println(Math.round(getSquare(4, 5, 45) *10)/10d);
     }

}
    // END

