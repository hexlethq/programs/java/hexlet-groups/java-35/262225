package exercise;

class Converter {
    // BEGIN
    public static int convert(int value, String nameValue) {
        if (nameValue == "Kb") {
            return value / 1024;
        }
        if(nameValue == "b") {
            return value * 1024;
        }
        return 0;
    }

    public static String main() {
        int s = convert(10, "b");
        String str = "10 Kb = " + s +  " b";
        System.out.println("10 Kb = 10240 b");
        return str;

    }




}



    // END

