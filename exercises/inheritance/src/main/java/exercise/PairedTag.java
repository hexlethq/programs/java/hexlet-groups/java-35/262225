package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {

    String body;
    List<Tag> list;

    public PairedTag(String name, Map<String, String> attribute, String body, List<Tag> list) {
        super(name, attribute);
        this.body = body;
        this.list = list;
    }

    @Override
    public String toString() {
        StringBuilder x = new StringBuilder();
        x.append(super.toString())
                .append(body);
        for(Tag w : list) {
            x.append(w.toString());
        }
        x.append("</")
                .append(name)
                .append(">");


        return x.toString();
    }
}

// END
