package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
public class Tag {
    String name;
    Map<String,String> attribute;

    public Tag(String name, Map<String, String> attribute) {
        this.name = name;
        this.attribute = attribute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getAttribute() {
        return attribute;
    }

    public void setAttribute(Map<String, String> attribute) {
        this.attribute = attribute;
    }

    public String toString() {
        StringBuilder x = new StringBuilder();
        x.append("<").append(name).append(" ");
        for (Map.Entry<String, String> entry : attribute.entrySet()) {
            x.append(entry.getKey())
                    .append("=")
                    .append("\"")
                    .append(entry.getValue())
                    .append("\"")
                    .append(" ");
        }
        x.setLength(x.length() -1);
        x.append(">");
        return x.toString();

    }
}
// END
