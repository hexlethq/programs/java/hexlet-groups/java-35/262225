package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {

    private String errorCode;
    private String message;

    public NegativeRadiusException(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public NegativeRadiusException() {

    }

    public NegativeRadiusException(String s) {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
// END
