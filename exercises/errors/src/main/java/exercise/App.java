package exercise;

// BEGIN
public class App {
    public static void main(String[] args) throws NegativeRadiusException {

        Point point = new Point(2, 3);
        Circle circle = new Circle(point, -1);
        App.printSquare(circle);
    }

    public static void printSquare(Circle circle) {
        try {
            System.out.println(Math.round(circle.getSquare()));
            System.out.println("Вычисление окончено");
        } catch (NegativeRadiusException e) {
            System.out.println("Не удалось посчитать площадь");
            System.out.println("Вычисление окончено");
        }
    }
}
// END
