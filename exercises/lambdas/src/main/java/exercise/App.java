package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static void main(String[] args) {

        String[][] image3 = {
                {"@", "@"},
                {"@", "|"},
                {"—", "|"},
                {"@", "|"},
                {"@", "@"},
        };
        String[][] enlargedImage3 = App.enlargeArrayImage(image3);
        System.out.println(Arrays.deepToString(enlargedImage3)); // =>
    }

    public static String[][] enlargeArrayImage(String[][] inputArr) {
        return Arrays.stream(inputArr)
                .map(z -> Arrays.stream(z).flatMap(a -> Stream.of(a,a)).toArray(String[]::new))
                .flatMap(x -> Stream.of(x, x))
                .toArray(String[][]::new);
    }
}
// END
