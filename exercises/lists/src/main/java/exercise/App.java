package exercise;

import java.util.ArrayList;


// BEGIN
public class App {
    public static boolean scrabble(String letters, String word) {

        ArrayList<Character> lettersList = new ArrayList<>();
        ArrayList<Character> wordList = new ArrayList<>();

        for (char symbol : letters.toLowerCase().toCharArray()) {
            lettersList.add(symbol);
        }

        for (char symbol : word.toLowerCase().toCharArray()) {
            wordList.add(symbol);
        }

        for (int i = 0; i < wordList.size(); i++) {
            if (!lettersList.remove(wordList.get(i))) {
                return false;
            }
        }
        return true;
    }
}
