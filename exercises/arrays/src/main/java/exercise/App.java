package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] inputArray) {
        int arrayLenght = inputArray.length;
        int[] reverseArr = new int[arrayLenght];
        for(int i = 0, k = inputArray.length -1 ; i < inputArray.length; i ++ , k--)
            reverseArr[i] = inputArray[k];
        return reverseArr;
    }

    public static int mult(int[] inputArray){
        int multiplicationNum = 1;
        for (int x: inputArray) {
            multiplicationNum = multiplicationNum * x;
        }
        return multiplicationNum;
    }
    // END

    // Домашнее задание
    public static int[] flattenMatrix(int[][] inputMatrix) {
        int rowLength = 0;
        for (int k = 0; k < inputMatrix.length; k++) {
            rowLength += inputMatrix[k].length;
        }

        int[] oneDimensionalArr = new int[rowLength];

        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix[i].length; j++)
                oneDimensionalArr[i * (inputMatrix[i].length) + j] = inputMatrix[i][j];
        }
        return oneDimensionalArr;
    }
}
