package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*";
        String cropString = cardNumber.substring(cardNumber.length() -4);
        System.out.println(stars.repeat(starsCount) + cropString);

        // END
    }
}
