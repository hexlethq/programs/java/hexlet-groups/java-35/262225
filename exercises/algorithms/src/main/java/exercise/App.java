package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] inputArr) {

        boolean sorted = false;
        int temp;
        while(!sorted) {

            sorted = true;

            for (int i = 0; i < inputArr.length - 1; i++) {
                if (inputArr[i] > inputArr[i + 1]) {
                    sorted = false;
                    temp = inputArr[i];
                    inputArr[i] = inputArr[i + 1];
                    inputArr[i + 1] = temp;
                }
            }
        }
        return inputArr;
    }
    // END
}
